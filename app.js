const path = require('path');
const express = require('express');
const bodyParser = require('body-parser');

const app = express();
const routes = require(path.join(__dirname, 'routes'));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(routes);
app.set('port', 3000)


// satic files
app.use(express.static(path.join(__dirname, 'public')));

module.exports = app;