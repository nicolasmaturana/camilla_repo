const express = require('express');
const router = express.Router();

const cronController = require('../controllers/arduinoController');

const middleware = (req, res, next) => {
    console.log("****************Middleware****************");
    next();
}

/** Agrega una nueva tarea al scheduler */
router.get('/savedata', middleware, cronController.saveData);

module.exports = router;