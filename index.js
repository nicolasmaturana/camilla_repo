const app = require('./app');

const init = async () => {
    // bootstraping the app
    await app.listen(app.get('port'));
    console.log(`server on port ${app.get('port')}`);
}

init();